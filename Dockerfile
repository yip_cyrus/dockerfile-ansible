# Use the office image as a parent image.
FROM ubuntu:latest

MAINTAINER Yip (Free Hong Kong)

ENV DEBIAN_FRONTEND=noninteractive
ENV ANSIBLE_HOST_KEY_CHECKING=False

RUN apt-get update &&\
    apt-get install -yqq gnupg2 python3-pip sshpass git openssh-client python-dev libkrb5-dev krb5-user && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean

RUN python3 -m pip install --upgrade pip cffi && \
    pip install ansible && \
    pip install pywinrm[kerberos] && \
    pip install --upgrade pywinrm